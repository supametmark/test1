from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse



def calculator(request):
    if request.method == 'POST':
        num1 = int(request.POST["first_num"])
        num2 = int(request.POST["last_num"])
        input_o = request.POST['operator']
        if input_o == "+":
            answer = str(num1 + num2)
        if input_o == "-":
            answer = str(num1 - num2)
        if input_o == "*":
            answer = str(num1 * num2)
        if input_o == "/":
            answer = str(num1 / num2)
        return render(request, 'calculator/result.html', {'num1': num1,'num2': num2, 'input_o': input_o ,'answer': " = "+answer})
    return render(request, 'calculator/calculator.html')
    







